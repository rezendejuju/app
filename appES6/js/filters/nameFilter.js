angular.module("listaTelefonica").filter("name",function(){
    //quando colocou "name" é o nome que deu para o filtro.
    return input=>{
       let listaDeNomes = input.split(" ");
       //map pega a lista de nomes e para cada elemento deriva um novo arrar
       let listaDeNomesFormatada = listaDeNomes.map(nome=>{
            //em vermelho é expressao regular, regexp
            if(/(da|de|do|das|dos)/.test(nome)) return nome;    
            return nome.charAt(0).toUpperCase() + nome.substring(1).toLowerCase(); 
        });
       return listaDeNomesFormatada.join(" ");
    };
});