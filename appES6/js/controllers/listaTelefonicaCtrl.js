  angular.module("listaTelefonica").controller("listaTelefonicaCtrl", MyController);
  
  
function MyController($scope,$filter, contatosAPI,operadorasAPI, serialGenerator) {
            //console.log(serialGenerator.generate());    
            $scope.app ="Lista Telefônica";
            $scope.contatos = [];
            $scope.operadoras = [];
            
            let carregarContatos = () => {
				contatosAPI.getContatos().then(response=>{
                    $scope.contatos = response.data;
                    }).catch(data=> {
                        $scope.error ="Não foi possível carregar os dados!";
				});
            };
            
            let carregarOperadoras  = () => {
               operadorasAPI.getOperadoras().then(response=> {
                     $scope.operadoras = response.data;
                });
			};

            $scope.adicionarContato = (contato) =>{
                let s = serialGenerator.generate();
                //alert(s);
                contato.serial = s;
                contato.data = new Date();
                contatosAPI.saveContato(contato).then(data=> {
                delete $scope.contato;
                $scope.contatoForm.$setPristine();
			    carregarContatos();
		        });
	        };
            $scope.apagarContato =  (contatos) => {
             $scope.contatos = contatos.filter(contato=>{
                 if(!contato.selecionado)  return contato;
             });
            
           };
           $scope.isContatoSelecionado =  (contatos) => {
              return contatos.some(contato=>{
                    return contato.selecionado;
               });
           };
         
           $scope.ordenarPor = (campo) => {
            $scope.criterioDeOrdenacao = campo;
            $scope.direcaoDaOrdenacao = !$scope.direcaoDaOrdenacao;
            }
        
        carregarContatos();
        carregarOperadoras();
}