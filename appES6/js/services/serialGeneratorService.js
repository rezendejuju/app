angular.module("listaTelefonica").provider("serialGenerator", function (config) {
	console.log(config);
	let _length = 10;
	
	this.getLength =  () => {
		return _length
	};

	this.setLength =  (length) => {
		_length = length;
	};

	this.$get =  () => {
        //quase similar ao factory, vai  expor essa opercao generate.
		return {
			generate: ()=> {
				let serial = "";
				while(serial.length < 20) {
					serial += String.fromCharCode(Math.floor(Math.random() * 64) + 32);
				}
				return serial;
			}

		};
	};
});

