angular.module("listaTelefonica").directive("uiAlert", function () {
	return {
        templateUrl: "view/alert.html",
		replace: true,
		//E- é de elemento, A é de atributo
		restrict: "AE",
		scope: {
			title: "@",
		},
		transclude: true
	
	};
});