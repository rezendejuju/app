angular.module("listaTelefonica").factory("contatosAPI", function ($http, config) {
//cria serviço utilizando o factory( fabrica, fabrica algo) registando o contato
//disponivel para injecao.
    var _getContatos = function (){
        return $http.get(config.baseUrl +  "/contatos");
    };
   
    var _saveContato = function (contato){
        return $http.post(config.baseUrl +  "/contatos",contato);
    };
   
    // esse return é como se fosse o que retorna da fabrica, o que produz de servico
    return{
        getContatos: _getContatos,
        saveContato: _saveContato
      
    };
});