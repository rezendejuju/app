angular.module("listaTelefonica").service("operadorasAPI", function ($http,config) {
	//service é uma funcao construtora;
	this.getOperadoras = function () {
        return $http.get(config.baseUrl + "/operadoras");
	};
});