angular.module("listaTelefonica").provider("serialGenerator", function (config) {
	console.log(config);
	var _length = 10;
	
	this.getLength = function () {
		return _length
	};

	this.setLength = function (length) {
		_length = length;
	};

	this.$get = function () {
        //quase similar ao factory, vai  expor essa opercao generate.
		return {
			generate: function () {
				var serial = "";
				while(serial.length < 20) {
					serial += String.fromCharCode(Math.floor(Math.random() * 64) + 32);
				}
				return serial;
			}

		};
	};
});

