angular.module("listaTelefonica").filter("ellipsis", function () {
	return function (input, size) {
        //esse size é para parametrizar quantas letras vão aparecer
        if (input.length <= size) return input;
        //... só pra quando o nome não couber o || 2 é o default da funcao
		var output = input.substring(0,(size || 2)) + "...";
		return output;
	};
});